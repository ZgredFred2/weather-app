export type AirQuality = {
  /** Carbon Monoxide (μg/m3) (float) */
  co: number;
  /** Ozone (μg/m3) (float) */
  o3: number;
  /** Nitrogen dioxide (μg/m3) (float) */
  no2: number;
  /** Sulphur dioxide (μg/m3) (float) */
  so2: number;
  /** PM2.5 (μg/m3) (float) */
  pm2_5: number;
  /** PM10 (μg/m3) (float) */
  pm10: number;
  // changed: us_epa_index -> us-epa-index
  /**
   * US - EPA standard. (integer)
   * 1 means Good
   * 2 means Moderate
   * 3 means Unhealthy for sensitive group
   * 4 means Unhealthy
   * 5 means Very Unhealthy
   * 6 means Hazardous
   */
  "us-epa-index": number;
  // changed:  gb_defra_index -> gb-defra-index
  /**
   * UK Defra Index (integer) @see {@link https://www.weatherapi.com/docs/#intro-aqi}
   */
  "gb-defra-index": number;
};

export type Autocomplete = Location[];

export type Location = {
  /** Latitude in decimal degree (float) */
  lat: number;
  /** Longitude in decimal degree (float) */
  lon: number;
  /** Location name */
  name: string;
  /** Region or state of the location, if available */
  region: string;
  /** Location country */
  country: string;
  /** Time zone name */
  tz_id: string;
  /** Local date and time in unix time (integer) */
  localtime_epoch: number;
  /** Local date and time */
  localtime: string;
};
export type ForecastDay = {
  /**	Forecast date */
  date: string;
  /** Forecast date as unix time (int) */
  date_epoch: number;
  /** See day element */
  day: DayElement;
  /** See astro element */
  astro: AstroElement;
  /** See hour element */
  hour: HourElement[];
};

export type CurrentWeather = {
  /** Local time when the real time data was updated */
  last_updated: string;
  /** Local time when the real time data was updated in unix time (int) */
  last_updated_epoch: number;
  /** Temperature in celsius (float) */
  temp_c: number;
  /** Temperature in fahrenheit (float) */
  temp_f: number;
  /** Feels like temperature in celsius (float) */
  feelslike_c: number;
  /** Feels like temperature in fahrenheit (float) */
  feelslike_f: number;
  condition: {
    /** Weather condition text */
    text: string;
    /** Weather icon url */
    icon: string;
    /** Weather condition unique code (int) */
    code: number;
  };
  /** Wind speed in miles per hour (float) */
  wind_mph: number;
  /** Wind speed in kilometer per hour (float) */
  wind_kph: number;
  /** Wind direction in degrees (int) */
  wind_degree: number;
  /** Wind direction as 16 point compass. e.g.: NSW */
  wind_dir: string;
  /** Pressure in millibars (float) */
  pressure_mb: number;
  /** Pressure in inches (float) */
  pressure_in: number;
  /** Precipitation amount in millimeters (float) */
  precip_mm: number;
  /** Precipitation amount in inches (float) */
  precip_in: number;
  /** Humidity as percentage (int) */
  humidity: number;
  /** Cloud cover as percentage (int) */
  cloud: number;
  /** 1 = Yes 0 = No Whether to show day condition icon or night icon (int) */
  is_day: number;
  /** UV Index (float) */
  uv: number;
  /** Wind gust in miles per hour (float) */
  gust_mph: number;
  /** Wind gust in kilometer per hour (float) */
  gust_kph: number;
  /** Visibility in kilometer (float) */
  vis_km: number;
  /** Visibility in miles (float) */
  vis_miles: number;
  /** Air quality data */
  air_quality?: AirQuality;
};

export type DayElement = {
  /** Maximum temperature in celsius for the day (float) */
  maxtemp_c: number;
  /** Maximum temperature in fahrenheit for the day (float) */
  maxtemp_f: number;
  /** Minimum temperature in celsius for the day (float) */
  mintemp_c: number;
  /** Minimum temperature in fahrenheit for the day (float) */
  mintemp_f: number;
  /** Average temperature in celsius for the day (float) */
  avgtemp_c: number;
  /** Average temperature in fahrenheit for the day (float) */
  avgtemp_f: number;
  /** Maximum wind speed in miles per hour (float) */
  maxwind_mph: number;
  /** Maximum wind speed in kilometer per hour (float) */
  maxwind_kph: number;
  /** Total precipitation in milimeter (float) */
  totalprecip_mm: number;
  /** Total precipitation in inches (float) */
  totalprecip_in: number;
  /** Average visibility in kilometer (float) */
  avgvis_km: number;
  /** Average visibility in miles (float) */
  avgvis_miles: number;
  /** Average humidity as percentage (int) */
  avghumidity: number;
  condition: {
    /** Weather condition text */
    text: string;
    /** Weather condition icon */
    icon: string;
    /** Weather condition code (int) */
    code: number;
  };
  // new values
  /** UV Index (float) */
  uv: number;
  /** Total snow in centimiters (float) */
  totalsnow_cm: number;
};
export type AstroElement = {
  /** Sunrise time */
  sunrise: string;
  /** Sunset time */
  sunset: string;
  /** Moonrise time */
  moonrise: string;
  /** Moonset time */
  moonset: string;
  /** Moon phases. Value returned:
New Moon
Waxing Crescent
First Quarter
Waxing Gibbous
Full Moon
Waning Gibbous
Last Quarter
Waning Crescent */
  moon_phase: string;
  // changed: number -> string
  /** Moon illumination as % (float) */
  moon_illumination: string;
};
export type HourElement = {
  /** Time as epoch (int) */
  time_epoch: number;
  /** Date and time */
  time: string;
  /** Temperature in celsius (float) */
  temp_c: number;
  /** Temperature in fahrenheit (float) */
  temp_f: number;
  condition: {
    /** Weather condition text */
    text: string;
    /** Weather condition icon */
    icon: string;
    /** Weather condition code (int) */
    code: number;
  };
  /** Maximum wind speed in miles per hour (float) */
  wind_mph: number;
  /** Maximum wind speed in kilometer per hour (float) */
  wind_kph: number;
  /** Wind direction in degrees (int) */
  wind_degree: number;
  /** Wind direction as 16 point compass. e.g.: NSW */
  wind_dir: string;
  /** Pressure in millibars (float) */
  pressure_mb: number;
  /** Pressure in inches (float) */
  pressure_in: number;
  /** Precipitation amount in millimeters (float) */
  precip_mm: number;
  /** Precipitation amount in inches (float) */
  precip_in: number;
  /** Humidity as percentage (int) */
  humidity: number;
  /** Cloud cover as percentage (int) */
  cloud: number;
  /** Feels like temperature as celcius (float) */
  feelslike_c: number;
  /** Feels like temperature as fahrenheit (float) */
  feelslike_f: number;
  /** Windchill temperature in celcius (float) */
  windchill_c: number;
  /** Windchill temperature in fahrenheit (float) */
  windchill_f: number;
  /** Heat index in celcius (float) */
  heatindex_c: number;
  /** Heat index in fahrenheit (float) */
  heatindex_f: number;
  /** Dew point in celcius (float) */
  dewpoint_c: number;
  /** Dew point in fahrenheit (float) */
  dewpoint_f: number;
  /** 1 = Yes 0 = No, Will it will rain or not (int) */
  will_it_rain: number;
  /** 1 = Yes 0 = No, Will it snow or not (int) */
  will_it_snow: number;
  /** 1 = Yes 0 = No, Whether to show day condition icon or night icon (int) */
  is_day: number;
  /** Visibility in kilometer (float) */
  vis_km: number;
  /** Visibility in miles (float) */
  vis_miles: number;
  /** Chance of rain as percentage (int) */
  chance_of_rain: number;
  /** Chance of snow as percentage (int) */
  chance_of_snow: number;
  /** Wind gust in miles per hour (float) */
  gust_mph: number;
  /** Wind gust in kilometer per hour (float) */
  gust_kph: number;
  // new element
  /** UV Index (float) */
  uv: number;
};

export type WeatherResponse = {
  location: Location;
  current: CurrentWeather;
  forecast: {
    forecastday: ForecastDay[];
  };
};

const x = {
  location: {
    name: "Lublin",
    region: "",
    country: "Poland",
    lat: 51.25,
    lon: 22.57,
    tz_id: "Europe/Warsaw",
    localtime_epoch: 1672646897,
    localtime: "2023-01-02 9:08",
  },
  current: {
    last_updated_epoch: 1672646400,
    last_updated: "2023-01-02 09:00",
    temp_c: 7.3,
    temp_f: 45.1,
    is_day: 1,
    condition: {
      text: "Sunny",
      icon: "//cdn.weatherapi.com/weather/64x64/day/113.png",
      code: 1000,
    },
    wind_mph: 9.4,
    wind_kph: 15.1,
    wind_degree: 204,
    wind_dir: "SSW",
    pressure_mb: 1023.0,
    pressure_in: 30.2,
    precip_mm: 0.0,
    precip_in: 0.0,
    humidity: 89,
    cloud: 16,
    feelslike_c: 4.6,
    feelslike_f: 40.3,
    vis_km: 10.0,
    vis_miles: 6.0,
    uv: 3.0,
    gust_mph: 19.7,
    gust_kph: 31.7,
    air_quality: {
      co: 263.70001220703125,
      no2: 6.099999904632568,
      o3: 24.700000762939453,
      so2: 3.200000047683716,
      pm2_5: 9.399999618530273,
      pm10: 10.899999618530273,
      "us-epa-index": 1,
      "gb-defra-index": 1,
    },
  },
  forecast: {
    forecastday: [
      {
        date: "2023-01-02",
        date_epoch: 1672617600,
        day: {
          maxtemp_c: 11.0,
          maxtemp_f: 51.8,
          mintemp_c: 6.4,
          mintemp_f: 43.5,
          avgtemp_c: 7.9,
          avgtemp_f: 46.3,
          maxwind_mph: 13.0,
          maxwind_kph: 20.9,
          totalprecip_mm: 0.0,
          totalprecip_in: 0.0,
          totalsnow_cm: 0.0,
          avgvis_km: 10.0,
          avgvis_miles: 6.0,
          avghumidity: 86.0,
          condition: {
            text: "Sunny",
            icon: "//cdn.weatherapi.com/weather/64x64/day/113.png",
            code: 1000,
          },
          uv: 3.0,
        },
        astro: {
          sunrise: "07:34 AM",
          sunset: "03:34 PM",
          moonrise: "12:18 PM",
          moonset: "03:15 AM",
          moon_phase: "Waxing Gibbous",
          moon_illumination: "69",
        },
        hour: [
          {
            time_epoch: 1672614000,
            time: "2023-01-02 00:00",
            temp_c: 7.2,
            temp_f: 45.0,
            is_day: 0,
            condition: {
              text: "Partly cloudy",
              icon: "//cdn.weatherapi.com/weather/64x64/night/116.png",
              code: 1003,
            },
            wind_mph: 10.3,
            wind_kph: 16.6,
            wind_degree: 222,
            wind_dir: "SW",
            pressure_mb: 1024.0,
            pressure_in: 30.23,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 88,
            cloud: 27,
            feelslike_c: 4.2,
            feelslike_f: 39.6,
            windchill_c: 4.2,
            windchill_f: 39.6,
            heatindex_c: 7.2,
            heatindex_f: 45.0,
            dewpoint_c: 5.3,
            dewpoint_f: 41.5,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 21.7,
            gust_kph: 34.9,
            uv: 1.0,
          },
          {
            time_epoch: 1672617600,
            time: "2023-01-02 01:00",
            temp_c: 7.0,
            temp_f: 44.6,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 9.6,
            wind_kph: 15.5,
            wind_degree: 218,
            wind_dir: "SW",
            pressure_mb: 1024.0,
            pressure_in: 30.23,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 88,
            cloud: 12,
            feelslike_c: 4.2,
            feelslike_f: 39.6,
            windchill_c: 4.2,
            windchill_f: 39.6,
            heatindex_c: 7.0,
            heatindex_f: 44.6,
            dewpoint_c: 5.2,
            dewpoint_f: 41.4,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 20.1,
            gust_kph: 32.4,
            uv: 1.0,
          },
          {
            time_epoch: 1672621200,
            time: "2023-01-02 02:00",
            temp_c: 6.9,
            temp_f: 44.4,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 9.6,
            wind_kph: 15.5,
            wind_degree: 216,
            wind_dir: "SW",
            pressure_mb: 1024.0,
            pressure_in: 30.23,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 88,
            cloud: 18,
            feelslike_c: 4.0,
            feelslike_f: 39.2,
            windchill_c: 4.0,
            windchill_f: 39.2,
            heatindex_c: 6.9,
            heatindex_f: 44.4,
            dewpoint_c: 5.1,
            dewpoint_f: 41.2,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 20.1,
            gust_kph: 32.4,
            uv: 1.0,
          },
          {
            time_epoch: 1672624800,
            time: "2023-01-02 03:00",
            temp_c: 6.6,
            temp_f: 43.9,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 9.2,
            wind_kph: 14.8,
            wind_degree: 217,
            wind_dir: "SW",
            pressure_mb: 1023.0,
            pressure_in: 30.22,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 89,
            cloud: 15,
            feelslike_c: 3.8,
            feelslike_f: 38.8,
            windchill_c: 3.8,
            windchill_f: 38.8,
            heatindex_c: 6.6,
            heatindex_f: 43.9,
            dewpoint_c: 4.9,
            dewpoint_f: 40.8,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.2,
            gust_kph: 31.0,
            uv: 1.0,
          },
          {
            time_epoch: 1672628400,
            time: "2023-01-02 04:00",
            temp_c: 6.5,
            temp_f: 43.7,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 9.4,
            wind_kph: 15.1,
            wind_degree: 216,
            wind_dir: "SW",
            pressure_mb: 1023.0,
            pressure_in: 30.22,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 89,
            cloud: 15,
            feelslike_c: 3.6,
            feelslike_f: 38.5,
            windchill_c: 3.6,
            windchill_f: 38.5,
            heatindex_c: 6.5,
            heatindex_f: 43.7,
            dewpoint_c: 4.8,
            dewpoint_f: 40.6,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.7,
            gust_kph: 31.7,
            uv: 1.0,
          },
          {
            time_epoch: 1672632000,
            time: "2023-01-02 05:00",
            temp_c: 6.4,
            temp_f: 43.5,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 9.2,
            wind_kph: 14.8,
            wind_degree: 211,
            wind_dir: "SSW",
            pressure_mb: 1023.0,
            pressure_in: 30.2,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 89,
            cloud: 24,
            feelslike_c: 3.5,
            feelslike_f: 38.3,
            windchill_c: 3.5,
            windchill_f: 38.3,
            heatindex_c: 6.4,
            heatindex_f: 43.5,
            dewpoint_c: 4.7,
            dewpoint_f: 40.5,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.2,
            gust_kph: 31.0,
            uv: 1.0,
          },
          {
            time_epoch: 1672635600,
            time: "2023-01-02 06:00",
            temp_c: 6.6,
            temp_f: 43.9,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 9.8,
            wind_kph: 15.8,
            wind_degree: 218,
            wind_dir: "SW",
            pressure_mb: 1023.0,
            pressure_in: 30.2,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 88,
            cloud: 21,
            feelslike_c: 3.6,
            feelslike_f: 38.5,
            windchill_c: 3.6,
            windchill_f: 38.5,
            heatindex_c: 6.6,
            heatindex_f: 43.9,
            dewpoint_c: 4.7,
            dewpoint_f: 40.5,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 20.6,
            gust_kph: 33.1,
            uv: 1.0,
          },
          {
            time_epoch: 1672639200,
            time: "2023-01-02 07:00",
            temp_c: 6.4,
            temp_f: 43.5,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 9.2,
            wind_kph: 14.8,
            wind_degree: 214,
            wind_dir: "SW",
            pressure_mb: 1023.0,
            pressure_in: 30.2,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 89,
            cloud: 19,
            feelslike_c: 3.5,
            feelslike_f: 38.3,
            windchill_c: 3.5,
            windchill_f: 38.3,
            heatindex_c: 6.4,
            heatindex_f: 43.5,
            dewpoint_c: 4.7,
            dewpoint_f: 40.5,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.2,
            gust_kph: 31.0,
            uv: 1.0,
          },
          {
            time_epoch: 1672642800,
            time: "2023-01-02 08:00",
            temp_c: 6.5,
            temp_f: 43.7,
            is_day: 1,
            condition: {
              text: "Sunny",
              icon: "//cdn.weatherapi.com/weather/64x64/day/113.png",
              code: 1000,
            },
            wind_mph: 8.9,
            wind_kph: 14.4,
            wind_degree: 204,
            wind_dir: "SSW",
            pressure_mb: 1023.0,
            pressure_in: 30.2,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 91,
            cloud: 12,
            feelslike_c: 3.7,
            feelslike_f: 38.7,
            windchill_c: 3.7,
            windchill_f: 38.7,
            heatindex_c: 6.5,
            heatindex_f: 43.7,
            dewpoint_c: 5.1,
            dewpoint_f: 41.2,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 18.8,
            gust_kph: 30.2,
            uv: 3.0,
          },
          {
            time_epoch: 1672646400,
            time: "2023-01-02 09:00",
            temp_c: 7.3,
            temp_f: 45.1,
            is_day: 1,
            condition: {
              text: "Sunny",
              icon: "//cdn.weatherapi.com/weather/64x64/day/113.png",
              code: 1000,
            },
            wind_mph: 9.4,
            wind_kph: 15.1,
            wind_degree: 204,
            wind_dir: "SSW",
            pressure_mb: 1023.0,
            pressure_in: 30.2,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 89,
            cloud: 16,
            feelslike_c: 4.6,
            feelslike_f: 40.3,
            windchill_c: 4.6,
            windchill_f: 40.3,
            heatindex_c: 7.3,
            heatindex_f: 45.1,
            dewpoint_c: 5.6,
            dewpoint_f: 42.1,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.7,
            gust_kph: 31.7,
            uv: 3.0,
          },
          {
            time_epoch: 1672650000,
            time: "2023-01-02 10:00",
            temp_c: 8.5,
            temp_f: 47.3,
            is_day: 1,
            condition: {
              text: "Sunny",
              icon: "//cdn.weatherapi.com/weather/64x64/day/113.png",
              code: 1000,
            },
            wind_mph: 10.7,
            wind_kph: 17.3,
            wind_degree: 204,
            wind_dir: "SSW",
            pressure_mb: 1023.0,
            pressure_in: 30.2,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 84,
            cloud: 15,
            feelslike_c: 5.8,
            feelslike_f: 42.4,
            windchill_c: 5.8,
            windchill_f: 42.4,
            heatindex_c: 8.5,
            heatindex_f: 47.3,
            dewpoint_c: 6.0,
            dewpoint_f: 42.8,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.7,
            gust_kph: 31.7,
            uv: 3.0,
          },
          {
            time_epoch: 1672653600,
            time: "2023-01-02 11:00",
            temp_c: 9.9,
            temp_f: 49.8,
            is_day: 1,
            condition: {
              text: "Sunny",
              icon: "//cdn.weatherapi.com/weather/64x64/day/113.png",
              code: 1000,
            },
            wind_mph: 12.1,
            wind_kph: 19.4,
            wind_degree: 203,
            wind_dir: "SSW",
            pressure_mb: 1022.0,
            pressure_in: 30.18,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 81,
            cloud: 12,
            feelslike_c: 7.3,
            feelslike_f: 45.1,
            windchill_c: 7.3,
            windchill_f: 45.1,
            heatindex_c: 9.9,
            heatindex_f: 49.8,
            dewpoint_c: 6.8,
            dewpoint_f: 44.2,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 18.1,
            gust_kph: 29.2,
            uv: 3.0,
          },
          {
            time_epoch: 1672657200,
            time: "2023-01-02 12:00",
            temp_c: 10.6,
            temp_f: 51.1,
            is_day: 1,
            condition: {
              text: "Sunny",
              icon: "//cdn.weatherapi.com/weather/64x64/day/113.png",
              code: 1000,
            },
            wind_mph: 12.8,
            wind_kph: 20.5,
            wind_degree: 204,
            wind_dir: "SSW",
            pressure_mb: 1021.0,
            pressure_in: 30.16,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 77,
            cloud: 10,
            feelslike_c: 8.1,
            feelslike_f: 46.6,
            windchill_c: 8.1,
            windchill_f: 46.6,
            heatindex_c: 10.6,
            heatindex_f: 51.1,
            dewpoint_c: 6.7,
            dewpoint_f: 44.1,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 18.1,
            gust_kph: 29.2,
            uv: 4.0,
          },
          {
            time_epoch: 1672660800,
            time: "2023-01-02 13:00",
            temp_c: 11.0,
            temp_f: 51.8,
            is_day: 1,
            condition: {
              text: "Sunny",
              icon: "//cdn.weatherapi.com/weather/64x64/day/113.png",
              code: 1000,
            },
            wind_mph: 13.0,
            wind_kph: 20.9,
            wind_degree: 207,
            wind_dir: "SSW",
            pressure_mb: 1021.0,
            pressure_in: 30.14,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 75,
            cloud: 9,
            feelslike_c: 8.6,
            feelslike_f: 47.5,
            windchill_c: 8.6,
            windchill_f: 47.5,
            heatindex_c: 11.0,
            heatindex_f: 51.8,
            dewpoint_c: 6.8,
            dewpoint_f: 44.2,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 17.4,
            gust_kph: 28.1,
            uv: 4.0,
          },
          {
            time_epoch: 1672664400,
            time: "2023-01-02 14:00",
            temp_c: 10.6,
            temp_f: 51.1,
            is_day: 1,
            condition: {
              text: "Partly cloudy",
              icon: "//cdn.weatherapi.com/weather/64x64/day/116.png",
              code: 1003,
            },
            wind_mph: 11.9,
            wind_kph: 19.1,
            wind_degree: 210,
            wind_dir: "SSW",
            pressure_mb: 1020.0,
            pressure_in: 30.13,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 76,
            cloud: 26,
            feelslike_c: 8.2,
            feelslike_f: 46.8,
            windchill_c: 8.2,
            windchill_f: 46.8,
            heatindex_c: 10.6,
            heatindex_f: 51.1,
            dewpoint_c: 6.5,
            dewpoint_f: 43.7,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 17.2,
            gust_kph: 27.7,
            uv: 4.0,
          },
          {
            time_epoch: 1672668000,
            time: "2023-01-02 15:00",
            temp_c: 10.3,
            temp_f: 50.5,
            is_day: 1,
            condition: {
              text: "Sunny",
              icon: "//cdn.weatherapi.com/weather/64x64/day/113.png",
              code: 1000,
            },
            wind_mph: 11.2,
            wind_kph: 18.0,
            wind_degree: 208,
            wind_dir: "SSW",
            pressure_mb: 1020.0,
            pressure_in: 30.13,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 79,
            cloud: 15,
            feelslike_c: 8.0,
            feelslike_f: 46.4,
            windchill_c: 8.0,
            windchill_f: 46.4,
            heatindex_c: 10.3,
            heatindex_f: 50.5,
            dewpoint_c: 6.8,
            dewpoint_f: 44.2,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 16.6,
            gust_kph: 26.6,
            uv: 4.0,
          },
          {
            time_epoch: 1672671600,
            time: "2023-01-02 16:00",
            temp_c: 9.3,
            temp_f: 48.7,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 11.6,
            wind_kph: 18.7,
            wind_degree: 200,
            wind_dir: "SSW",
            pressure_mb: 1020.0,
            pressure_in: 30.13,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 83,
            cloud: 23,
            feelslike_c: 6.6,
            feelslike_f: 43.9,
            windchill_c: 6.6,
            windchill_f: 43.9,
            heatindex_c: 9.3,
            heatindex_f: 48.7,
            dewpoint_c: 6.7,
            dewpoint_f: 44.1,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.2,
            gust_kph: 31.0,
            uv: 1.0,
          },
          {
            time_epoch: 1672675200,
            time: "2023-01-02 17:00",
            temp_c: 8.9,
            temp_f: 48.0,
            is_day: 0,
            condition: {
              text: "Partly cloudy",
              icon: "//cdn.weatherapi.com/weather/64x64/night/116.png",
              code: 1003,
            },
            wind_mph: 12.5,
            wind_kph: 20.2,
            wind_degree: 203,
            wind_dir: "SSW",
            pressure_mb: 1020.0,
            pressure_in: 30.13,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 85,
            cloud: 29,
            feelslike_c: 6.0,
            feelslike_f: 42.8,
            windchill_c: 6.0,
            windchill_f: 42.8,
            heatindex_c: 8.9,
            heatindex_f: 48.0,
            dewpoint_c: 6.6,
            dewpoint_f: 43.9,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 20.6,
            gust_kph: 33.1,
            uv: 1.0,
          },
          {
            time_epoch: 1672678800,
            time: "2023-01-02 18:00",
            temp_c: 8.6,
            temp_f: 47.5,
            is_day: 0,
            condition: {
              text: "Partly cloudy",
              icon: "//cdn.weatherapi.com/weather/64x64/night/116.png",
              code: 1003,
            },
            wind_mph: 12.5,
            wind_kph: 20.2,
            wind_degree: 214,
            wind_dir: "SW",
            pressure_mb: 1020.0,
            pressure_in: 30.13,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 87,
            cloud: 30,
            feelslike_c: 5.6,
            feelslike_f: 42.1,
            windchill_c: 5.6,
            windchill_f: 42.1,
            heatindex_c: 8.6,
            heatindex_f: 47.5,
            dewpoint_c: 6.6,
            dewpoint_f: 43.9,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.5,
            gust_kph: 31.3,
            uv: 1.0,
          },
          {
            time_epoch: 1672682400,
            time: "2023-01-02 19:00",
            temp_c: 8.2,
            temp_f: 46.8,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 11.9,
            wind_kph: 19.1,
            wind_degree: 222,
            wind_dir: "SW",
            pressure_mb: 1021.0,
            pressure_in: 30.14,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 90,
            cloud: 22,
            feelslike_c: 5.2,
            feelslike_f: 41.4,
            windchill_c: 5.2,
            windchill_f: 41.4,
            heatindex_c: 8.2,
            heatindex_f: 46.8,
            dewpoint_c: 6.6,
            dewpoint_f: 43.9,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 17.4,
            gust_kph: 28.1,
            uv: 1.0,
          },
          {
            time_epoch: 1672686000,
            time: "2023-01-02 20:00",
            temp_c: 6.9,
            temp_f: 44.4,
            is_day: 0,
            condition: {
              text: "Partly cloudy",
              icon: "//cdn.weatherapi.com/weather/64x64/night/116.png",
              code: 1003,
            },
            wind_mph: 10.7,
            wind_kph: 17.3,
            wind_degree: 220,
            wind_dir: "SW",
            pressure_mb: 1020.0,
            pressure_in: 30.13,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 92,
            cloud: 27,
            feelslike_c: 3.8,
            feelslike_f: 38.8,
            windchill_c: 3.8,
            windchill_f: 38.8,
            heatindex_c: 6.9,
            heatindex_f: 44.4,
            dewpoint_c: 5.7,
            dewpoint_f: 42.3,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 18.8,
            gust_kph: 30.2,
            uv: 1.0,
          },
          {
            time_epoch: 1672689600,
            time: "2023-01-02 21:00",
            temp_c: 6.6,
            temp_f: 43.9,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 10.5,
            wind_kph: 16.9,
            wind_degree: 213,
            wind_dir: "SSW",
            pressure_mb: 1020.0,
            pressure_in: 30.12,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 93,
            cloud: 14,
            feelslike_c: 3.5,
            feelslike_f: 38.3,
            windchill_c: 3.5,
            windchill_f: 38.3,
            heatindex_c: 6.6,
            heatindex_f: 43.9,
            dewpoint_c: 5.5,
            dewpoint_f: 41.9,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.9,
            gust_kph: 32.0,
            uv: 1.0,
          },
          {
            time_epoch: 1672693200,
            time: "2023-01-02 22:00",
            temp_c: 6.5,
            temp_f: 43.7,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 9.6,
            wind_kph: 15.5,
            wind_degree: 232,
            wind_dir: "SW",
            pressure_mb: 1020.0,
            pressure_in: 30.13,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 92,
            cloud: 14,
            feelslike_c: 3.5,
            feelslike_f: 38.3,
            windchill_c: 3.5,
            windchill_f: 38.3,
            heatindex_c: 6.5,
            heatindex_f: 43.7,
            dewpoint_c: 5.3,
            dewpoint_f: 41.5,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 19.5,
            gust_kph: 31.3,
            uv: 1.0,
          },
          {
            time_epoch: 1672696800,
            time: "2023-01-02 23:00",
            temp_c: 6.7,
            temp_f: 44.1,
            is_day: 0,
            condition: {
              text: "Clear",
              icon: "//cdn.weatherapi.com/weather/64x64/night/113.png",
              code: 1000,
            },
            wind_mph: 10.1,
            wind_kph: 16.2,
            wind_degree: 235,
            wind_dir: "SW",
            pressure_mb: 1020.0,
            pressure_in: 30.13,
            precip_mm: 0.0,
            precip_in: 0.0,
            humidity: 90,
            cloud: 24,
            feelslike_c: 3.7,
            feelslike_f: 38.7,
            windchill_c: 3.7,
            windchill_f: 38.7,
            heatindex_c: 6.7,
            heatindex_f: 44.1,
            dewpoint_c: 5.2,
            dewpoint_f: 41.4,
            will_it_rain: 0,
            chance_of_rain: 0,
            will_it_snow: 0,
            chance_of_snow: 0,
            vis_km: 10.0,
            vis_miles: 6.0,
            gust_mph: 20.4,
            gust_kph: 32.8,
            uv: 1.0,
          },
        ],
      },
    ],
  },
} satisfies WeatherResponse;
