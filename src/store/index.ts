// store/store.js
import create from "zustand";
import { WeatherType } from "../server/trpc/router/weather";
import { UnitSystem } from "../types/unitSystem";
import { Location } from "../weatherApi/forecast";

type StoreState = {
  selectedLocation: Location | undefined;
  setSelectedLocation: (location: Location) => void;

  locationCurrentWeather: WeatherType["current"] | undefined;
  setLocationCurrentWeather: (weather: WeatherType["current"]) => void;

  searchedLocation: string;
  setSearchedLocation: (location: string) => void;

  unitSystem: UnitSystem;
  setUnitSystem: (unitSystem: UnitSystem) => void;
};

const useLocationStore = create<StoreState>((set) => ({
  selectedLocation: undefined,
  setSelectedLocation(location) {
    set((_state) => ({
      selectedLocation: location,
    }));
  },

  locationCurrentWeather: undefined,
  setLocationCurrentWeather: (weather: WeatherType["current"]) => {
    set((_state) => ({ locationCurrentWeather: weather }));
  },

  searchedLocation: "",
  setSearchedLocation(location) {
    set((_state) => ({
      searchedLocation: location,
    }));
  },

  unitSystem: "metric",
  setUnitSystem: (unitSystem: UnitSystem) => {
    set((_state) => ({ unitSystem }));
  },
}));

export default useLocationStore;
