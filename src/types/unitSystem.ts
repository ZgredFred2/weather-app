export type UnitSystem = "metric" | "imperial";

export type UnitData = {
  value: number;
  short: string;
};