import { type NextPage } from "next";
import { useEffect } from "react";
import { Footer } from "../components/Footer";
import { ForecastList } from "../components/ForecastList/ForecastList";
import { Heading } from "../components/Heading/Heading";
import { UnitsSelector } from "../components/UnitsSelector";
import { WeatherParamsWithUnits } from "../components/WeatherParamsHighlights/WithUnits";
import useLocationStore from "../store";
import { trpc } from "../utils/trpc";

const Home: NextPage = () => {
  const location = useLocationStore((state) => state.selectedLocation);
  const weather = trpc.weather.hello.useQuery(
    {
      location: location?.name ?? "London",
      days: 3,
      airQuality: true,
      includeHours: false,
    },
    {
      retry: 0,
      refetchOnWindowFocus: false,
      // one hour refetch
      refetchInterval: 1 * 1000 * 60 * 60,
    }
  );
  const setStoreLocation = useLocationStore(
    (state) => state.setSelectedLocation
  );
  const setStoreCurrentWeather = useLocationStore(
    (state) => state.setLocationCurrentWeather
  );

  console.log(weather.data);
  const apiJson = weather.data;
  useEffect(() => {
    if (weather.data) {
      setStoreLocation(weather.data.location);
    }

    if (weather.data?.current) {
      setStoreCurrentWeather(weather.data.current);
    }
  }, [weather.data]);

  const apiError = weather.error;
  return (
    <div className="font-Raleway text-[16px] font-medium text-[#E7E7EB]">
      <div className="flex h-full min-h-screen w-full flex-col md:flex-row">
        {/* h-screen ? */}
        <div className="w-full flex-grow bg-blue-500 md:basis-5/12">
          <Heading />
        </div>
        <div className=" flex w-full flex-col justify-between bg-[#100E1D] px-2 py-2 xl:px-20 xl:pt-10 xl:pb-6 ">
          <div>
            <div className="flex flex-row justify-end gap-2">
              <UnitsSelector />
            </div>
            <div className="pt-10" />
            <div className="flex flex-row flex-wrap justify-center gap-6">
              <ForecastList data={apiJson?.forecast.forecastday ?? []} />
            </div>

            <div className="pt-[52px] md:pt-[72px]" />

            <div className="flex flex-col items-center">
              <div className="flex flex-grow-0 flex-col items-center">
                <div className="self-start text-[24px] font-bold">
                  Today’s Hightlights
                </div>

                <div className="pt-8 " />
                <div className="flex flex-1 flex-col items-center justify-center gap-8 md:inline-grid md:w-full md:grid-cols-2 md:gap-12">
                  <WeatherParamsWithUnits data={apiJson?.current} />
                </div>
              </div>
            </div>
          </div>

          <div className="pt-8 " />
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default Home;
