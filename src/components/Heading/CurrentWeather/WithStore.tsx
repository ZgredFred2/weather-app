import { FC, Fragment } from "react";
import useLocationStore from "../../../store";
import { dateShorter } from "../../../utils/strings";
import { MyOmit } from "../../../utils/types";
import { extractUnits } from "../../../utils/unitSystem";
import { CurrentWeatherProps } from ".";
import { WithRoundedUnits } from "./WithRoundedUnits";

export type CurrentWeatherWithStoreProps = MyOmit<
  CurrentWeatherProps,
  // everything except data we prepare
  | "location"
  | "locationCountry"
  | "imgUrl"
  | "temp"
  | "todayString"
  | "conditionString"
>;

export const CurrentWeatherWithStore: FC<CurrentWeatherWithStoreProps> = (
  props
) => {
  const location = useLocationStore((state) => state.selectedLocation);
  const currentWeather = useLocationStore(
    (state) => state.locationCurrentWeather
  );
  const unitsSystem = useLocationStore((state) => state.unitSystem);
  const setLocation = useLocationStore((state) => state.setSearchedLocation);
  if (!currentWeather || !location) {
    console.log({ currentWeather, location });
    setLocation("London");
    return <Fragment />;
  }
  const units = extractUnits(unitsSystem)(currentWeather);
  return (
    <WithRoundedUnits
      {...props}
      {...units}
      // TODO: do it correctly
      todayString={dateShorter(new Date())}
      location={location.name}
      conditionString={currentWeather.condition.text}
      locationCountry={location.country}
      temp={units.temp}
      imgUrl={currentWeather.condition.icon}
    />
  );
};
