import { FC } from "react";
import { UnitData } from "../../../types/unitSystem";

export type CurrentWeatherProps = {
  onSearchSelect?: () => any;
  location: string;
  locationCountry: string;
  todayString: string;
  temp: UnitData;
  imgUrl: string;
  conditionString: string;
};

export const CurrentWeather: FC<CurrentWeatherProps> = (props) => {
  return (
    <div className="flex h-full flex-col items-center justify-between overflow-hidden  bg-[#1E213A] px-2 py-3 text-[#E7E7EB] xl:px-[46px] xl:py-[42px]">
      <div className="flex w-full flex-col">
        {/* search bar */}
        <div className="flex w-full flex-row items-center justify-between">
          <button
            onClick={props.onSearchSelect}
            className="bg-[#6E707A] px-2 py-[10px] shadow-[0px_4px_4px_rgba(0,0,0,0.25)] xl:px-[18px]"
          >
            Search for places
          </button>
          <button className="flex h-[40px] w-[40px] items-center justify-center rounded-full bg-[#6E707A] shadow-[0px_4px_4px_rgba(0,0,0,0.25)]">
            <svg className="h-[24px] w-[24px] fill-current">
              <path d="M12 22.95q-.425 0-.712-.287Q11 22.375 11 21.95v-1q-3.125-.35-5.362-2.587Q3.4 16.125 3.05 13h-1q-.425 0-.713-.288-.287-.287-.287-.712t.287-.713Q1.625 11 2.05 11h1q.35-3.125 2.588-5.363Q7.875 3.4 11 3.05v-1q0-.425.288-.713.287-.287.712-.287t.713.287q.287.288.287.713v1q3.125.35 5.363 2.587Q20.6 7.875 20.95 11h1q.425 0 .713.287.287.288.287.713t-.287.712q-.288.288-.713.288h-1q-.35 3.125-2.587 5.363Q16.125 20.6 13 20.95v1q0 .425-.287.713-.288.287-.713.287ZM12 19q2.9 0 4.95-2.05Q19 14.9 19 12q0-2.9-2.05-4.95Q14.9 5 12 5 9.1 5 7.05 7.05 5 9.1 5 12q0 2.9 2.05 4.95Q9.1 19 12 19Zm0-3q-1.65 0-2.825-1.175Q8 13.65 8 12q0-1.65 1.175-2.825Q10.35 8 12 8q1.65 0 2.825 1.175Q16 10.35 16 12q0 1.65-1.175 2.825Q13.65 16 12 16Zm0-2q.825 0 1.413-.588Q14 12.825 14 12t-.587-1.413Q12.825 10 12 10q-.825 0-1.412.587Q10 11.175 10 12q0 .825.588 1.412Q11.175 14 12 14Zm0-2Z" />
            </svg>
          </button>
        </div>

        <div className="pt-[21px]" />
        {/* graphics */}
        <div
          className="w-full"
          style={{
            position: "relative",
            height: "367px",
          }}
        >
          <div
            className="absolute h-full bg-center bg-repeat-x opacity-5"
            style={{
              backgroundImage: `url("/data/Cloud-background.png")`,
              // revert padding and remove element from flow
              left: "-46px",
              width: `calc(100% + 46px * 2)`,
            }}
          ></div>
          <div className="flex h-full w-full items-center justify-center">
            <img className="w-[240px]" src={props.imgUrl} />
          </div>
        </div>

        {/* <div className="pt-[29px]" /> */}
        {/* meteorologicalData */}
        <div className="flex flex-col items-center justify-center">
          <div>
            <span className="text-[144px] font-medium">{props.temp.value}</span>
            <span className="text-[40px] text-[#A09FB1] ">
              {props.temp.short}
            </span>
          </div>
          <div className="pt-[61px]" />
          <span className="text-[36px] font-semibold text-[#A09FB1]">
            {props.conditionString}
          </span>
        </div>
      </div>

      <div className="pt-[87px]" />
      {/* footer */}
      <div className="flex flex-col items-center justify-center text-[#A09FB1]">
        <div className="flex flex-row items-center justify-center gap-3">
          <div>Today</div>
          <div>•</div>
          <div>{props.todayString}</div>
        </div>
        <div className="pt-[20px]" />
        <div className="flex flex-row justify-center gap-1">
          <span className="material-icons">location_on</span>
          <span className="font-semibold">
            {props.location}, {props.locationCountry}
          </span>
        </div>
      </div>
    </div>
  );
};
