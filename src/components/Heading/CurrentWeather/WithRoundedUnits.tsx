import { FC } from "react";
import { CurrentWeather, CurrentWeatherProps } from ".";
import { transformProps, unitsRounder } from "../../../utils/component";

export type WithRoundedUnitsProps = CurrentWeatherProps;

export const WithRoundedUnits: FC<WithRoundedUnitsProps> = transformProps(
  CurrentWeather,
  (data) => unitsRounder(data, ["temp"])
);
