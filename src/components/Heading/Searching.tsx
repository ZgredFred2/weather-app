import { FC, useEffect, useState } from "react";
import useLocationStore from "../../store";
import { waitFor } from "../../utils";
import { trpc } from "../../utils/trpc";

export type SearchingCompProps = { onClose?: () => any };

export const SearchingComp: FC<SearchingCompProps> = (props) => {
  const locationStore = useLocationStore((state) => state);
  const [searchString, setSearchString] = useState(
    locationStore.searchedLocation
  );
  const [searchLocationString, setSearchLocationString] = useState("");
  const autocomplete = trpc.weather.autocomplete.useQuery(
    { locationString: searchLocationString },
    { refetchOnWindowFocus: false, retry: false }
  );

  useEffect(() => {
    let canceled = false;

    waitFor(500).then((_) => {
      if (!canceled) setSearchLocationString((_) => searchString);
    });

    return () => {
      canceled = true;
    };
  }, [searchString]);

  return (
    <div className="h-full bg-[#1E213A] px-[12px] py-[17px] text-[#E7E7EB] md:px-[46px] md:py-[20px]">
      {/* close button */}
      <div className="flex flex-row justify-end ">
        <button onClick={props.onClose}>
          <span className="material-icons text-[19px]">close</span>
        </button>
      </div>
      <div className="pt-[28px]"></div>
      <div className="flex flex-row items-center gap-[12px]">
        <div className="box-border flex flex-1 flex-row border-[1px] border-current p-[14px]">
          <div className="flex flex-row items-center gap-[9px]">
            <span className="material-icons">search</span>
            <input
              className="bg-inherit focus:outline-none"
              placeholder="search location"
              value={searchString}
              onChange={(e) => {
                setSearchString(e.target.value);
              }}
            />
          </div>
        </div>
        <button className="h-full bg-[#3C47E9] p-[14px] font-semibold">
          Search
        </button>
      </div>
      {autocomplete.data?.length !== 0 && (
        <div className="flex flex-col gap-5 pt-[52px]">
          {autocomplete.data?.map((location) => (
            <button
              key={location.name}
              className={
                `box-border flex flex-row justify-between border-[1px] border-transparent py-5 px-3 transition-all hover:border-[#616475]` +
                ` ${location === locationStore.selectedLocation
                  ? "border-[#3C47E9] hover:border-[#5762ff]"
                  : ""
                }`
              }
              onClick={() => {
                locationStore.setSelectedLocation(location);
                locationStore.setSearchedLocation(searchString);
                props.onClose?.();
              }}
            >
              {location.name}
              <span className="material-icons text-[#616475]">
                navigate_next
              </span>
            </button>
          ))}
        </div>
      )}
    </div>
  );
};
