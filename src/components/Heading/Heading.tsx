import { FC, useState } from "react";
import { CurrentWeatherWithStore } from "./CurrentWeather/WithStore";
import { SearchingComp } from "./Searching";

export type HeadingProps = unknown;
export const Heading: FC<HeadingProps> = (props) => {
  const [searching, setSearching] = useState(false);

  if (searching) return <SearchingComp onClose={() => setSearching(false)} />;
  return (
    <CurrentWeatherWithStore
      onSearchSelect={() => {
        setSearching(true);
      }}
    />
  );
};
