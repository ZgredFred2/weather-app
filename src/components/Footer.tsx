import { FC } from "react";

export type FooterProps = unknown;

export const Footer: FC<FooterProps> = (props) => {
  return (
    <div className="flex flex-col items-center text-[14px] text-[#A09FB1]">
      <div>
        Powered by{" "}
        <a href="https://www.weatherapi.com/" title="Free Weather API">
          WeatherAPI.com
        </a>
      </div>
      <div>created by ZgredFred - devChallenges.io</div>
    </div>
  );
};
