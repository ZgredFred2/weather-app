import { FC, Fragment } from "react";
import { WeatherType } from "../../server/trpc/router/weather";
import useLocationStore from "../../store";
import { dateShorter } from "../../utils/strings";
import { extractForecastUnits } from "../../utils/unitSystem";
import { ForecastCardProps } from "./ForecastCard";
import { ForecastWithRoundedNumbers } from "./ForecastWithRoundedNumbers";

export type ForecastListProps = {
  data: WeatherType["forecast"]["forecastday"];
};

export const ForecastList: FC<ForecastListProps> = (props) => {
  const unitSystem = useLocationStore((state) => state.unitSystem);
  return (
    <Fragment>
      {props.data.map((forecast, idx) => {
        const data: ForecastCardProps = {
          header:
            idx === 0
              ? "Today"
              : idx === 1
                ? "Tomorrow"
                : dateShorter(forecast.date),
          iconRef: forecast.day.condition.icon,
          ...extractForecastUnits(unitSystem)(forecast),
        };

        return <ForecastWithRoundedNumbers key={forecast.date} {...data} />;
      })}
    </Fragment>
  );
};
