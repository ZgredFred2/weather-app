import { FC } from "react";
import { UnitData } from "../../types/unitSystem";

export type ForecastCardProps = {
  header: string;
  iconRef: string;
  minValue: UnitData;
  maxValue: UnitData;
};

export const ForecastCard: FC<ForecastCardProps> = (props) => {
  return (
    <div
      key={props.header}
      className="flex min-w-fit basis-[115px] flex-col justify-between gap-6 rounded-md bg-[#1E213A] px-5 py-4"
    >
      {/* title + icon */}
      <div className="flex flex-col items-center justify-center">
        {props.header}
        <img src={props.iconRef} className="w-full" />
      </div>
      {/* values */}
      <div className="flex flex-row justify-between">
        <div>{props.maxValue.value + props.maxValue.short}</div>
        <div>{props.minValue.value + props.minValue.short}</div>
      </div>
    </div>
  );
};
