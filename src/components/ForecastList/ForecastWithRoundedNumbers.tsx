import { FC } from "react";
import { transformProps, unitsRounder } from "../../utils/component";
import { ForecastCardProps, ForecastCard } from "./ForecastCard";

export const ForecastWithRoundedNumbers: FC<ForecastCardProps> = transformProps(
  ForecastCard,
  (data) => unitsRounder(data, ["minValue", "maxValue"])
);
