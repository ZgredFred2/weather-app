import { FC, Fragment } from "react";
import useLocationStore from "../store";
import { celsuisString, farenheitString } from "../utils/unitSystem";
import { UnitButton } from "./UnitButton";

const unitsList = [celsuisString, farenheitString] as const;

export type UnitsSelectorProps = unknown;

const getStyle = (selected: boolean) =>
  selected ? "bg-[#E7E7EB] text-[#110E3C]" : "bg-[#585676] text-[#E7E7EB]";

export const UnitsSelector: FC<UnitsSelectorProps> = (props) => {
  const unitSystem = useLocationStore((state) => state.unitSystem);
  const setUnitSystem = useLocationStore((state) => state.setUnitSystem);

  return (
    <Fragment>
      {unitsList.map((unit) => {
        const style = getStyle(
          (unit === celsuisString && unitSystem === "metric") ||
          (unit === farenheitString && unitSystem === "imperial")
        );

        return (
          <UnitButton
            key={unit}
            unit={unit}
            size={40}
            className={`${style} text-[18px] transition-all duration-500 hover:brightness-125`}
            onClick={() => {
              if (unit === celsuisString) setUnitSystem("metric");
              else setUnitSystem("imperial");
            }}
          />
        );
      })}
    </Fragment>
  );
};
