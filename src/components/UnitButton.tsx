import { ButtonHTMLAttributes, DetailedHTMLProps, FC } from "react";

type UnitButtonProps = DetailedHTMLProps<
  ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
> & {
  unit: string;
  size: number;
  className?: string;
};
export const UnitButton: FC<UnitButtonProps> = (props) => {
  return (
    <button
      {...props}
      className={`h-[${props.size}px] w-[${props.size}px] rounded-full ${props.className}`}
    >
      {props.unit}
    </button>
  );
};
