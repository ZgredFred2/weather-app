import { FC } from "react";

export type BarProps = {
  value: number;
  color: string;
  height: string;
};

export const Bar: FC<BarProps> = (props) => {
  return (
    <div style={{ height: props.height }}>
      <div className="relative">
        {/* back bar */}
        <div
          style={{
            width: "100%",
            height: props.height,
            backgroundColor: "#E7E7EB",
            borderRadius: 9999,
            position: "absolute",
          }}
        />
        {/* front-indicator bar */}
        <div
          style={{
            // TODO: take min and max value, unit from props
            width: props.value + "%",
            height: props.height,
            borderRadius: 9999,
            backgroundColor: props.color,
            position: "absolute",
          }}
        />
      </div>
    </div>
  );
};
