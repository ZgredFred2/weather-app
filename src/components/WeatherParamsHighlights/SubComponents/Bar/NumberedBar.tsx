import { FC } from "react";
import { BarProps, Bar } from "./Bar";

export type NumberedBarProps = BarProps & {
  startingValue: string;
  midValue: string;
  endingValue: string;
  unit: string;
};

export const NumberedBar: FC<NumberedBarProps> = (props) => {
  return (
    <div className="text-[12px] font-bold">
      <div className="flex flex-row justify-between ">
        <div>{props.startingValue}</div>
        <div>{props.midValue}</div>
        <div>{props.endingValue}</div>
      </div>
      <Bar {...props} />
      <div className="flex flex-row justify-end">{props.unit}</div>
    </div>
  );
};
