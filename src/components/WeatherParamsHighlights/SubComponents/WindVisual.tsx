import { FC } from "react";

export type WindVisualProps = {
  angle: number;
  directionText: string;
};

export const WindVisual: FC<WindVisualProps> = (props) => {
  return (
    <div className="flex flex-row items-center justify-center gap-3 pt-1">
      <div className="flex h-8 w-8 flex-row items-center justify-center rounded-full bg-[#FFFFFF4D]">
        <span
          className="material-icons text-[16px]"
          style={{
            rotate: props.angle + "deg",
          }}
        >
          navigation
        </span>
      </div>
      <div>{props.directionText}</div>
    </div>
  );
};
