import { FC, Fragment } from "react";
import { WeatherType } from "../../server/trpc/router/weather";
import useLocationStore from "../../store";
import { extractUnits } from "../../utils/unitSystem";
import { WithRoundedUnits } from "./WithRoundedUnits";

export type WeatherParamsWithUnitsProps = { data?: WeatherType["current"] };

export const WeatherParamsWithUnits: FC<WeatherParamsWithUnitsProps> = (
  props
) => {
  const unitSystem = useLocationStore((state) => state.unitSystem);
  if (!props.data) return <Fragment />;

  const data = {
    ...props.data,
    ...extractUnits(unitSystem)(props.data),
  };
  return <WithRoundedUnits {...data} />;
};
