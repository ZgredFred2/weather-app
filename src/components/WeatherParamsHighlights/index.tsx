import { FC, Fragment } from "react";
import { NumberedBar } from "./SubComponents/Bar/NumberedBar";
import { WetherParamCard } from "./WetherParamCard";
import { WindVisual } from "./SubComponents/WindVisual";
import { UnitData } from "../../types/unitSystem";

export type WeatherParamsHightlightsProps = {
  wind: UnitData;
  humidity: UnitData;
  vis: UnitData;
  pressure: UnitData;
  wind_degree: number;
  wind_dir: string;
};

export const WeatherParamsHightlights: FC<WeatherParamsHightlightsProps> = (
  props
) => {
  return (
    <Fragment>
      {/* wind */}
      <WetherParamCard
        header="Wind Status"
        value={props.wind.value.toString()}
        unit={props.wind.short}
        appendix={
          <WindVisual
            directionText={props.wind_dir}
            angle={props.wind_degree}
          />
        }
      />
      {/* humidity */}
      <WetherParamCard
        header="Humidity"
        value={props.humidity.value.toString()}
        unit={props.humidity.short}
        appendix={
          <NumberedBar
            startingValue="0"
            midValue="50"
            endingValue="100"
            unit={props.humidity.short}
            height="8px"
            value={props.humidity.value}
            color="#FFEC65"
          />
        }
      />
      {/* visibility */}
      <WetherParamCard
        header="Visibility"
        value={props.vis.value.toString()}
        unit={props.vis.short}
        spaceUnit
      />
      {/* api pressure */}
      <WetherParamCard
        header="Air Pressure"
        value={props.pressure.value.toString()}
        unit={props.pressure.short}
        spaceUnit
      />
    </Fragment>
  );
};
