import { FC } from "react";

export type WeatherParamCardProps = {
  header: React.ReactNode;
  value: string;
  unit: string;
  spaceUnit?: boolean;
  appendix?: React.ReactNode;
};

export const WetherParamCard: FC<WeatherParamCardProps> = (props) => {
  return (
    <div className="flex h-full w-[230px] flex-col items-stretch rounded-md bg-[#1E213A] px-5 pb-2 pt-3 xl:w-[328px]  xl:px-12 xl:pt-5 xl:pb-4">
      <div className="flex flex-col items-center">
        <div>{props.header}</div>
        <div className="pt-2 pb-4">
          <span className="text-6xl font-bold xl:text-7xl">{props.value}</span>
          <span className="text-[36px]">
            {props.spaceUnit && " "}
            {props.unit}
          </span>
        </div>
      </div>
      {props.appendix && <div>{props.appendix}</div>}
    </div>
  );
};
