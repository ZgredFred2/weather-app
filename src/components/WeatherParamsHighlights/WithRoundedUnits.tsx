import { FC } from "react";
import { WeatherParamsHightlights, WeatherParamsHightlightsProps } from ".";
import { transformProps, unitsRounder } from "../../utils/component";

export type WithRoundedUnitsProps = WeatherParamsHightlightsProps;

export const WithRoundedUnits: FC<WithRoundedUnitsProps> = transformProps(
  WeatherParamsHightlights,
  (data) => unitsRounder(data, ["vis", "wind", "pressure"])
);
