import { WeatherType } from "../server/trpc/router/weather";
import { UnitData, UnitSystem } from "../types/unitSystem";

export const celsuisString = "℃";
export const farenheitString = "℉";

export const extractUnits =
  (unitSystem: UnitSystem) => (data: WeatherType["current"]) => {
    if (unitSystem === "metric")
      return {
        temp: { value: data.temp_c, short: celsuisString },
        feelslike: { value: data.feelslike_c, short: celsuisString },
        wind: { value: data.wind_kph, short: "kph" },
        gust: { value: data.gust_kph, short: "kph" },
        vis: { value: data.vis_km, short: "km" },
        pressure: { value: data.pressure_mb, short: "mb" },
        precip: { value: data.precip_mm, short: "mm" },

        humidity: { value: data.humidity, short: "%" },
      } as const satisfies Record<string, UnitData>;
    return {
      temp: { value: data.temp_f, short: farenheitString },
      feelslike: { value: data.feelslike_f, short: farenheitString },
      wind: { value: data.wind_mph, short: "mph" },
      gust: { value: data.gust_mph, short: "mph" },
      vis: { value: data.vis_miles, short: "miles" },
      pressure: { value: data.pressure_in, short: "in" },
      precip: { value: data.precip_in, short: "in" },

      humidity: { value: data.humidity, short: "%" },
    } as const satisfies Record<string, UnitData>;
  };

export const extractForecastUnits =
  (unitSystem: UnitSystem) =>
    (data: WeatherType["forecast"]["forecastday"][0]) => {
      if (unitSystem === "metric") {
        return {
          minValue: {
            value: data.day.mintemp_c,
            short: celsuisString,
          },
          maxValue: {
            value: data.day.maxtemp_c,
            short: celsuisString,
          },
        } as const;
      }
      return {
        minValue: {
          value: data.day.mintemp_f,
          short: farenheitString,
        },
        maxValue: {
          value: data.day.maxtemp_f,
          short: farenheitString,
        },
      } as const;
    };
