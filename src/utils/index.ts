export const waitFor = async (time: number) => {
  await new Promise((r) => setTimeout(r, time));
};
