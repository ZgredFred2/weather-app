const options = {
  weekday: "short",
  month: "short",
  day: "numeric",
} as const;

const _dateShorterOfDate = (date: Date) =>
  date.toLocaleDateString("en-US", options);

export const dateShorter = (date: Date | string) => {
  if (typeof date === "string") {
    return _dateShorterOfDate(new Date(date));
  }
  return _dateShorterOfDate(date);
};
