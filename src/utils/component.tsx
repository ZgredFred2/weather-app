import { FC } from "react";
import { UnitData } from "../types/unitSystem";

// <Base baseProps />, transformer: otherProps => baseProps
//      => (prop: otherProps) => <Base transform(baseProps) />
export const transformProps = <BaseProps extends {}, OtherProps>(
  Comp: FC<BaseProps>,
  transformer: (props: OtherProps) => BaseProps
) => {
  const MyComp = (props: OtherProps) => {
    return <Comp {...transformer(props)} />;
  };
  return MyComp;
};


const unitRounder = (data: UnitData): UnitData => {
  return { ...data, value: Math.round(data.value) };
};

// TODO: make it safe
export const unitsRounder = <T extends {}, P extends (keyof T)[]>(
  data: T,
  keys: P
) => {
  const newData = keys.reduce((prev, curr) => {
    prev[curr] = unitRounder(data[curr] as UnitData);
    return prev;
  }, {} as Record<P[number], UnitData | undefined>);
  return {
    ...data,
    ...newData,
  };
};