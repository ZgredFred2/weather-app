import { TRPCError } from "@trpc/server";
import { TRPC_ERROR_CODE_KEY } from "@trpc/server/rpc";
import { z } from "zod";
import { MyOmit } from "../../../utils/types";
import {
  Autocomplete,
  ForecastDay,
  WeatherResponse,
} from "../../../weatherApi/forecast";

import { router, publicProcedure } from "../trpc";

const boolToYesNo = (value: boolean) => (value ? "yes" : "no");

export type WeatherType = MyOmit<WeatherResponse, "forecast"> & {
  forecast: {
    forecastday: Array<
      MyOmit<ForecastDay, "astro" | "hour"> & Partial<ForecastDay>
    >;
  };
};

export const weatherRouter = router({
  autocomplete: publicProcedure
    .input(
      z.object({
        locationString: z.string({
          required_error: "location string required",
        }),
      })
    )
    .query(async ({ input }) => {
      const url = `${process.env.WEATHER_API_URL}/search.json?key=${process.env.WEATHER_API_KEY}&q=${input.locationString}`;
      console.log(url);
      const things = await fetch(url);
      if (things.status !== 200) {
        throw new TRPCError({
          message: "error status:" + things.status.toString(),
          code: things.statusText
            .toUpperCase()
            .replaceAll(" ", "_") as TRPC_ERROR_CODE_KEY,
        });
      }
      let result = (await things.json()) as Autocomplete;
      return result;
    }),
  hello: publicProcedure
    .input(
      z.object({
        location: z.string({ required_error: "location requred" }),
        days: z
          .number()
          .min(1, {
            message:
              "Number of days for weather forecast cannot be less than 1.",
          })
          .max(14, {
            message:
              "Number of days for weather forecast cannot be higher than 14.",
          })
          .default(1),
        airQuality: z.boolean().default(false),
        alerts: z.boolean().default(false),
        includeHours: z.boolean().default(false),
        includeAstro: z.boolean().default(false),
      })
    )
    .query(async ({ input }) => {
      const url = `${process.env.WEATHER_API_URL}/forecast.json?key=${process.env.WEATHER_API_KEY
        }&q=${input.location}&days=${input.days}&aqi=${boolToYesNo(
          input.airQuality
        )}&alerts=${boolToYesNo(input.alerts)}`;
      console.log(url);
      const things = await fetch(url);
      if (things.status !== 200) {
        throw new TRPCError({
          message: "error status:" + things.status.toString(),
          code: things.statusText
            .toUpperCase()
            .replaceAll(" ", "_") as TRPC_ERROR_CODE_KEY,
        });
      }
      let result = (await things.json()) as WeatherType;
      result.forecast.forecastday.forEach((forecast) => {
        if (!input.includeHours) delete forecast.hour;
        if (!input.includeAstro) delete forecast.astro;
      });
      return result;
    }),
});
