import { router } from "../trpc";
import { weatherRouter } from "./weather";

export const appRouter = router({
  weather: weatherRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
