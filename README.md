<!-- Please update value in the {}  -->

<h1 align="center">Weather App</h1>

[:point_right: Demo](https://zgredfred-weather-app.vercel.app/)

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Features](#features)
- [How to use](#how-to-use)

<!-- OVERVIEW -->

## Overview

![screenshot](docs/demo.gif)
Weather App provides detailed weather forecasts for today and the upcoming days, including information on humidity, wind direction, visibility, and air pressure. App allows you to switch between metric and imperial units.

### Initalized With

- [T3 App](https://create.t3.gg/)

### Built With

<!-- This section should list any major frameworks that you built your project using. Here are a few examples.-->

- [React](https://reactjs.org/)
- [Tailwind](https://tailwindcss.com/)
- [NextJs](https://nextjs.org/)
- [tRPC](https://trpc.io/)
- [Zustand](https://github.com/pmndrs/zustand)

### Used external API

- [WeatherApi](https://www.weatherapi.com/)

## Features

<!-- List the features of your application or follow the template. Don't share the figma file here :) -->

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges/mM1UIenRhK808W8qmLWv) challenge. The challenge was to build an application to complete the given user stories.

## How To Use

<!-- Example: -->

To clone and run this application, you'll need

- [Git](https://git-scm.com)
- [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer.
- [WeatherApi](https://www.weatherapi.com/) account with api key

From your command line:

```bash
# Clone this repository
git clone https://gitlab.com/ZgredFred2/weather-app.git

# enter weather-app directory
cd weather-app

# Install dependencies
npm install
```

Make `.env` file with following content:

```
WEATHER_API_URL=https://api.weatherapi.com/v1
WEATHER_API_KEY=<your_weatherapi_key>
```

```bash
# Run NextJs app in development mode
npm run dev
```
